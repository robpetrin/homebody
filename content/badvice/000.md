---
title: "A Word of Badvice (#000)"
date: 2017-07-01T23:58:29-04:00
draft: false
thumbnail: "images/Badvice.png"
podcasts: ["Badvice with Dr. Mike"]
#change these three values to match the name of your show (use the case of the defaults!)
#story goes down here -- use Markdown!
---
In this **debut** edition, Mike suggests methods for improving your love life, and how to fit in in high school. Mike also takes some time to give instructions on how **YOU** can become the next beneficiary of the some bad advice from Dr. Mike*.

**Mike** is the Chair of the Badvisory Board (and also it's only member). As with every show on **Homebody Media**, our success is tied to our community participation. You steer each show with your suggestions, comments, questions, complaints, and feedback. You're involvement isn't just welcome -- it's required.

Want to support our show, but aren't sure how? You should...

* Subscribe, rate five stars, and review us in **[iTunes](http://YOURSHOWNAME)**
* Follow us on **[Twitter](http://twitter.com/badvice)**
* Follow **[Homebody Media](http://twitter.com/homebodymedia)** as well to stay up to date on all of your favorite conversations!
