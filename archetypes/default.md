---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
draft: true
thumbnail: "images/Header.png"
podcasts: ["news"]
#change these three values to match the name of your show (use the case of the defaults!)
#story goes down here -- use Markdown!
---
Article Content

**Names on the podcast this week** sentence

As with every show on **Homebody Media**, our success is tied to our community participation. You steer each show with your suggestions, comments, questions, complaints, and feedback. You're involvement isn't just welcome -- it's required.

Want to support our show, but aren't sure how? You should...

* Subscribe, rate five stars, and review us in **[iTunes](http://YOURSHOWNAME)**
* Follow us on **[Twitter](http://twitter.com/YOURSHOWNAME)**
* Follow **[Homebody Media](http://twitter.com/homebodymedia)** as well to stay up to date on all of your favorite conversations!
